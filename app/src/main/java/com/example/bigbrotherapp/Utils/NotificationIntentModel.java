package com.example.bigbrotherapp.Utils;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

public class NotificationIntentModel implements Parcelable {

    public static final Creator<NotificationIntentModel> CREATOR = new Creator<NotificationIntentModel>() {
        @Override
        public NotificationIntentModel createFromParcel(Parcel in) {
            return new NotificationIntentModel(in);
        }

        @Override
        public NotificationIntentModel[] newArray(int size) {
            return new NotificationIntentModel[size];
        }
    };
    private int idNotification;
    private Intent notificationIntent;
    private PendingIntent contentIntent;

    public NotificationIntentModel(int idNotification, Intent notificationIntent, PendingIntent contentIntent) {
        this.idNotification = idNotification;
        this.notificationIntent = notificationIntent;
        this.contentIntent = contentIntent;
    }

    protected NotificationIntentModel(Parcel in) {
        idNotification = in.readInt();
        notificationIntent = in.readParcelable(Intent.class.getClassLoader());
        contentIntent = in.readParcelable(PendingIntent.class.getClassLoader());
    }

    public int getIdNotification() {
        return idNotification;
    }

    public Intent getNotificationIntent() {
        return notificationIntent;
    }

    public PendingIntent getContentIntent() {
        return contentIntent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idNotification);
        dest.writeParcelable(notificationIntent, flags);
        dest.writeParcelable(contentIntent, flags);
    }
}
