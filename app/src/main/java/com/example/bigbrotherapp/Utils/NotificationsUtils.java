package com.example.bigbrotherapp.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.example.bigbrotherapp.R;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationsUtils {
    public static final String EXTRA_INTENT = "pendingIntent";
    public static final String EXTRA_ID_NOTIFICATION = "idNotification";
    private static String SILENT_CHANNEL = "channel";

    public static void createNotification(Context context
            , String title
            , String description
            , PendingIntent pendingIntent
            , int idNotification
            , String nameChannel
            , int drawable) {

        long[] vibration = {123};

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(SILENT_CHANNEL/*DEBUG!SilentChannel*/, nameChannel,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(true);

            notificationManager.createNotificationChannel(channel);

            NotificationOreo(notificationManager, context, pendingIntent, idNotification, title, description,drawable);
        } else {
            NotificationStandart(notificationManager, context, pendingIntent, idNotification, title, description, drawable);
        }


    }

    private static void NotificationOreo(NotificationManager notificationManager, Context context,
                                         PendingIntent contentIntent, int id, String title, String description, int drawable) {

        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), drawable);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, SILENT_CHANNEL);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(description)
                .setLights(Color.BLUE, 500, 500)
                .setLargeIcon(icon)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(icon)
                        .bigLargeIcon(icon))
//                .setVibrate(vibration);
                .addAction(R.mipmap.ic_launcher, "Перейти", contentIntent);

        Notification notification = builder.build();
        notificationManager.notify(id, notification);
    }

    private static void NotificationStandart(NotificationManager notificationManager, Context context,
                                             PendingIntent contentIntent, int idNotification, String title, String description, int drawable) {

        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), drawable);


        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(description)
                .setLights(Color.BLUE, 500, 500)
                .setLargeIcon(icon)
                .setStyle(new Notification.BigPictureStyle()
                        .bigPicture(icon)
                        .bigLargeIcon(icon))
//                .setVibrate(vibration)
                .addAction(R.mipmap.ic_launcher, "Перейти", contentIntent);
        Notification notification = builder.build();
        notificationManager.notify(idNotification, notification);

    }

}
