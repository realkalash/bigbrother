package com.example.bigbrotherapp.Recievers;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.wifi.WifiManager;

import com.example.bigbrotherapp.MainActivity;
import com.example.bigbrotherapp.R;
import com.example.bigbrotherapp.Utils.NotificationsUtils;

import static com.example.bigbrotherapp.Utils.Constants.EXTRA_GPS_ID;

public class GPSReceiver extends BroadcastReceiver {

    boolean isGpsEnabled;

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(context,
                        EXTRA_GPS_ID, notificationIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);


        if (LocationManager.PROVIDERS_CHANGED_ACTION.equals(action)) {
            isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (isGpsEnabled) {
                NotificationsUtils.createNotification(context,
                        "GPS",
                        "Большой брат созерцает тебя",
                        pendingIntent,
                        0,
                        "GPS Manager",
                        R.drawable.sss);
            } else {
                NotificationsUtils.createNotification(context,
                        "GPS",
                        "Большой брат не созерцает тебя",
                        pendingIntent,
                        0,
                        "GPS Manager",
                        R.drawable.sss);

            }

        }
    }
}
