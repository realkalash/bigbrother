package com.example.bigbrotherapp.Recievers;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.widget.Toast;

import com.example.bigbrotherapp.MainActivity;
import com.example.bigbrotherapp.R;
import com.example.bigbrotherapp.Utils.NotificationIntentModel;
import com.example.bigbrotherapp.Utils.NotificationsUtils;

import static com.example.bigbrotherapp.Utils.Constants.*;

public class WiFIReciever extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(context,
                        EXTRA_WIFI_ID, notificationIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

        if (action.equals(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION)) {
            if (intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED, false)) {
                //do stuff
                NotificationsUtils.createNotification(context, "WiFi", "Большой брат следит за тобой",
                        pendingIntent, 1,"Wifi Manager", R.drawable.sss);

            } else {
                // wifi connection was lost
                NotificationsUtils.createNotification(context, "WiFi", "Большой брат не следит за тобой",
                        pendingIntent, 1,"Wifi Manager",R.drawable.sss);


            }
        }
    }
}
