package com.example.bigbrotherapp;

import android.content.Context;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bigbrotherapp.Recievers.GPSReceiver;
import com.example.bigbrotherapp.Recievers.WiFIReciever;

public class MainActivity extends AppCompatActivity {
    Button srvcButton;
    TextView status, wifi, gps;

    GPSReceiver GPSReceiver = new GPSReceiver();
    WiFIReciever wiFIReciever = new WiFIReciever();


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        srvcButton = findViewById(R.id.srvcButton);

        status = findViewById(R.id.am_status);
        wifi = findViewById(R.id.am_wifi);
        gps = findViewById(R.id.am_gps);

        status.setText("Status");

        SetTextRows();


        srvcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                IntentFilter wifiGpsFilter = new IntentFilter();
                wifiGpsFilter.addAction(LocationManager.PROVIDERS_CHANGED_ACTION);
//                wifiGpsFilter.addAction(LocationManager.NETWORK_PROVIDER);

                registerReceiver(GPSReceiver, wifiGpsFilter);


               IntentFilter wifiFilter = new IntentFilter();
               wifiFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);

                registerReceiver(wiFIReciever, wifiFilter);

            }
        });


    }

    private void SetTextRows() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = wifiManager.isWifiEnabled();

        if (isGpsEnabled)
            gps.setText("GPS enabled");
        else gps.setText("GPS disabled");
        if (isNetworkEnabled)
            wifi.setText("Network enabled");
        else wifi.setText("Network disabled");

        if (isGpsEnabled = isNetworkEnabled = true)
            status.setText("Большой брат видит тебя");
        else if (isNetworkEnabled != isGpsEnabled)
            status.setText("Большой брат присматривает за тобой");
        else status.setText("Большой брат тебя не видит, но ощущает");
    }
}
